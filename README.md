# 2025-03-10,10:01:06  
## [AdRules](https://adrules.top/) 📖[WTFPL](https://raw.githubusercontent.com/Cats-Team/AdRules/main/LICENSE)  
`807827353a147c91e22a61e18b1abd37` adblock.txt [源地址](https://adrules.top/adblock.txt) [镜像地址](https://gitea.com/lemon399/AdRules/raw/branch/main/adblock.txt)

`c185f35912debcc82b1ebcedb85b8e37` adblock_lite.txt [源地址](https://adrules.top/adblock_lite.txt) [镜像地址](https://gitea.com/lemon399/AdRules/raw/branch/main/adblock_lite.txt)

`14f30de530f7c63c689d2ca564a8defc` adblock_plus.txt [源地址](https://adrules.top/adblock_plus.txt) [镜像地址](https://gitea.com/lemon399/AdRules/raw/branch/main/adblock_plus.txt)


## [几十KB的轻量规则](https://github.com/damengzhu/banad)  
`8084c5d36f8cd2b8daacbe1e269fb803` jiekouAD.txt [源地址](https://raw.githubusercontent.com/damengzhu/banad/main/jiekouAD.txt) [镜像地址](https://gitea.com/lemon399/AdRules/raw/branch/main/jiekouAD.txt)


## [ABP Merge Rules](https://github.com/damengzhu/abpmerge)  
`26565982274bc3bbc24701d5f9d78093` abpmerge.txt [源地址](https://raw.githubusercontent.com/damengzhu/abpmerge/main/abpmerge.txt) [镜像地址](https://gitea.com/lemon399/AdRules/raw/branch/main/abpmerge.txt)

`ae916b20c20b01d13ac5f59699d40a11` CSSRule.txt [源地址](https://raw.githubusercontent.com/damengzhu/abpmerge/main/CSSRule.txt) [镜像地址](https://gitea.com/lemon399/AdRules/raw/branch/main/CSSRule.txt)


## [混合规则](https://lingeringsound.github.io/adblock/) 📖[Apache-2.0](https://raw.githubusercontent.com/lingeringsound/adblock/master/LICENSE)  
`13c7e192f61a309cf4921c6715ac962c` adblock [源地址](https://raw.githubusercontent.com/lingeringsound/adblock/master/adblock) [镜像地址](https://gitea.com/lemon399/AdRules/raw/branch/main/adblock)

`dd6e98d97d5c5fe563d7ba08d3cbf6c8` adblock_lite [源地址](https://raw.githubusercontent.com/lingeringsound/adblock/master/adblock_lite) [镜像地址](https://gitea.com/lemon399/AdRules/raw/branch/main/adblock_lite)


